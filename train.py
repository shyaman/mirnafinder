#!/usr/bin/env python
# coding: utf-8

# import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# hydra libraries
import hydra
from omegaconf import DictConfig, OmegaConf

from Bio import SeqIO
from pathlib import Path

# Get some classifiers to evaluate
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA

import logging

logger = logging.getLogger(__name__)

import wandb


class miRNABaseDataset:
    def __init__(self, path: Path = Path("data/miRBase")):
        self.path = path

        # read train set
        self.trainSet = pd.read_csv(path / "trainingDatasetFeat.csv")

        # read test set
        self.testSet = pd.read_csv(path / "testingDatasetFeat.csv")


@hydra.main(config_path="configs", config_name="default")
def train(cfg: DictConfig):

    np.random.seed(cfg.seed)

    logger.info(OmegaConf.to_yaml(cfg))

    # load the dataset
    dataset_path = Path(hydra.utils.get_original_cwd()) / Path(cfg.data.path)
    data = miRNABaseDataset(dataset_path)

    # shuffle
    data.trainSet = data.trainSet.sample(frac=1, random_state=cfg.seed)

    # training classes
    train_y = data.trainSet["class"]

    # training features
    train_feat = data.trainSet.iloc[:, 3:-1]

    # testing classes
    test_y = data.testSet["class"]

    # testing features
    test_feat = data.testSet.iloc[:, 3:-2]

    # scaler transformation
    train_x = train_feat.values
    min_max_scaler = MinMaxScaler()
    min_max_scaler = min_max_scaler.fit(train_x)

    train_feat_scaled = min_max_scaler.transform(train_x)
    train_feat_scaled = pd.DataFrame(train_feat_scaled)
    train_feat_scaled.columns = train_feat.columns

    # pca transform
    pca = PCA(random_state=cfg.seed, n_components=122)
    pcafit = pca.fit(train_feat_scaled)

    pca_train_feat = pcafit.transform(train_feat_scaled)

    labels = ["pseudo-mirna", "mirna"]

    ####### Random forest classifier ########

    rf_model = RandomForestClassifier(random_state=cfg.seed)
    rf_model.fit(pca_train_feat, train_y)

    test_feat_scaled = min_max_scaler.transform(test_feat)
    pca_test_feat = pcafit.transform(test_feat_scaled)

    y_pred = rf_model.predict(pca_test_feat)
    y_probas = rf_model.predict_proba(pca_test_feat)

    # train and wandb logging
    run1 = wandb.init(
        entity="mirnafinder", project="mirnafinder-v2", name="RandomForest", reinit=True
    )
    wandb.sklearn.plot_classifier(
        rf_model,
        pca_train_feat,
        pca_test_feat,
        train_y,
        test_y,
        y_pred,
        y_probas,
        labels,
        False,
        "RandomForest",
        None,
    )
    run1.finish()

    ####### MLP classifier ########
    nn_model = MLPClassifier(random_state=cfg.seed, hidden_layer_sizes=(100, 50, 20))
    nn_model.fit(pca_train_feat, train_y)

    test_feat_scaled = min_max_scaler.transform(test_feat)
    pca_test_feat = pcafit.transform(test_feat_scaled)

    y_pred = nn_model.predict(pca_test_feat)
    y_probas = nn_model.predict_proba(pca_test_feat)

    # train and wandb logging
    run2 = wandb.init(
        entity="mirnafinder", project="mirnafinder-v2", name="MLP", reinit=True
    )
    wandb.sklearn.plot_classifier(
        nn_model,
        pca_train_feat,
        pca_test_feat,
        train_y,
        test_y,
        y_pred,
        y_probas,
        labels,
        False,
        "MLP",
        None,
    )
    run2.finish()


if __name__ == "__main__":
    train()
